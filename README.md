# infra

**Дипломный проект  -  инфраструктурный код**

Этот репозиторий содержит код инфраструктуры (IaC) для приложения skillbox-diploma.
Репозиторий нужно развернуть на управляющей машине.

## Состав инфраструктуры:

1. Сервер приложения;
2. Балансировщик;
3. Gitlab runner для CI/CD кода приложения
4. Сервер мониторинга
5. Зона для домена приложения (в данном случае `boangri.fun`) 

![Состав инфраструктуры](diploma_project.png "Состав инфраструктуры")

## Требования к управляющей машине

Должны быть установлены:

1. AWS CLI;
2. Сконфигурированы credentials для ASW CLI;
3. Terraform;
4. Ansible.

## Развертывание инфраструктуры с помощью Terraform

Параметры инфраструктуры (токены, пароли) в целях безопасности вынесены из репозитория.
Они должны задаваться как переменные окружения

Пример (данные модифицированы, работать не будут):
```
export REG_TOKEN="g6Rz_WPPrgUxymnN"  # токен для регистрации раннера в репозитории boangri/skillbox-diploma
export PASSPHRASE="kN2xsF0xrt"  # фраза для расшифровки приватного RSA ключа
export PROJECT_ID=29375964
export STATE_NAME=gitlab_runner
export USERNAME=boangri
export ACCESS_TOKEN=ycLEgsFTYVcA3J4  # токен для доступа в хранилище tfstate
export TF_VAR_domain="boangri.fun"
```
Убедитесь что вы в ветке `main`

### Создайте доменную зону :
```
$ cd terraform/zone
$ ./init.sh   # terraform init
$ terraform apply -auto-approve
```
В консоли `Route 53` надо убедиться что создалась зона для домена `TF_VAR_domain` и зарегистрировать имена nameservers
у регистратора домена (в моем случае это jino.ru) 

### Разверните сервер приложения и балансировщик (для development environment):
```
$ cd terraform/dev-env
$ ./init.sh   # terraform init
$ terraform apply -auto-approve
```
Должна создасться CNAME запись в зоне (app.boangri.fun) для указания на адрес 
балансировщика.

### Разверните сервер приложения и балансировщик (для production environment):
```
$ cd terraform/prod-env
$ ./init.sh   # terraform init
$ terraform apply -auto-approve
```
Должна создасться CNAME запись в зоне (prod.boangri.fun) для указания на адрес 
балансировщика.

### Разверните gitlab runner для обслуживания CI/CD репозитория skillbox-diploma:

```
$ cd terraform/gitlab-runner-service
$ ./init.sh  # terraform init
$ terraform apply -auto-approve
```
Должна создасться A-запись в зоне (gitlab-ranner.boangri.fun) для указания адреса раннера. 

### Разверните сервер мониторинга:

сервер развернут в облаке Yandex Cloud (для опыта работы с разными провайдерами)

```
$ export TF_VAR_token="AQAAAAAALdgoAATuwXAkN2xsF0xrtgO17"  # токен для доступа в Yandex Cloud
$ export TF_VAR_cloud_id="b1glipjic5a85a8brp" # токен для доступа в Yandex Cloud
$ export TF_VAR_folder_id="b1go9agnugprgdq2qm" # токен для доступа в Yandex Cloud
$
$ cd terraform/monitoring-yc
$ ./init.sh  # terraform init
$ terraform apply -auto-approve
```

## Ansible

Дальнейшие настройки инфраструктуры делаются с помощью Ansible.

Перейдите в каталог `ansible` в корневом каталоге проекта: 

```
$ cd ansible
$ ./ips
```

`ips` выведет список IP-адресов серверов в группах. Эти адреса надо занести в конфигурацию prometheus


```
$ vi roles/prometheus/files/prometheus.yml # - редактируем вдреса
$ ansible -m ping -u ubuntu all # Опционально - проверить доступность серверов
$ ansible-playbook -b -v -u ubuntu app.yml # конфигурирование тестового сервера приложения
$ ansible-playbook -b -v -u ubuntu prod.yml # конфигурирование production сервера приложения
$ ansible-playbook -b -v -u ubuntu gitlab-runner.yml # установка раннера
$ ansible-playbook -b -v -u ubuntu monitoring.yml # установка prometheus и grafana
$ ansible-playbook -b -v -u ubuntu create_users.yml # Опционально - создаст дополнительные эккаунты `ansible` и `boris` на всех серверах
```
Для Grafana нужно вручную сменить начальный пароль, инициировать 
источники данных Prometheus & Loki и импортировать дашборд `dashboard.json` из каталога `grafana`

http://grafana.boangri.fun:3000

## Check

1. Проверьте что создался runner для репозитория skillbox-diploma
2. Проверьте сервера мониторинга

http://monitoring.boangri.fun:9090

http://grafana.boangri.fun:3000

3. После развертывания приложения (из репозитория boangri/skillbox-diploma) должны стать доступными

Деплой на dev и prod происходит автоматически при коммите в ветку `main` в репозитории `boangri/skillbox-diploma`

3.1 Тестовое окружение

http://app.boangri.fun

http://app.boangri.fun/health

http://app.boangri.fun/metrics

3.2 Production

http://prod.boangri.fun

http://prod.boangri.fun/health

http://prod.boangri.fun/metrics
