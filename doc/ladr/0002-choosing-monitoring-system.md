# 2. Choosing Monitoring System

Date: 2021-09-11

## Status

Accepted

## Context

We need to choose some monitoring system.

## Decision

We will use [Prometheus](https://prometheus.io/) as it completely meets our requirements - it is popular, handy to use, has large community.
It was considered in lessons of our course, so we know how to deal with it, and there should be no issues during installation and configuration.
Besides, it is free (economical aspect)

## Consequences

We will have to choose a visualization system for displaying collected metrics.
