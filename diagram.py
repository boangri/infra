# https://diagrams.mingrammer.com

from diagrams import Diagram, Cluster, Edge
from diagrams.aws.compute import EC2, ECS
from diagrams.aws.database import RDS
from diagrams.aws.network import ELB
from diagrams.aws.network import Route53
from diagrams.onprem.monitoring import Grafana, Prometheus
from diagrams.programming.language import Go

with Diagram("Diploma project", show=False):
    
    app = Go("Приложение на golang")

    with Cluster("Provider: AWS"):
        with Cluster("Production Env"):
            prod_lb = ELB("prod.boangri.fun")
            with Cluster("Prod Auto Scaling Group"):
                prod1, prod2 = ECS("prod1"), ECS("prod2") 
                prod_group = [prod1, prod2]
            prod_lb >> Edge(color="darkorange") >> prod_group

        with Cluster("Development Env"):
            dev_lb = ELB("app.boangri.fun")
            with Cluster("Dev Auto Scaling Group"):
                dev = ECS("app")
                dev_group = [dev]
            dev_lb >> Edge(color="darkorange") >> dev_group
        runner = EC2("gitlab runner")
        dev << Edge(color="green") << runner
        prod1 << Edge(color="green") << runner
        prod2 << Edge(color="green") << runner
        dns = Route53("boangri.fun zone")

    with Cluster("Provider: Yandex Cloud"):           
        with Cluster("Monitoring"):              
            metrics = Prometheus("prometheus")
            dev << Edge(color="blue") << metrics << Grafana("grafana")
            prod1 << Edge(color="blue") << metrics
            prod2 << Edge(color="blue") << metrics

