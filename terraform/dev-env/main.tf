terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

# Configure the DigitalOcean Provider
provider "digitalocean" {
  token = var.do_token
}

provider "aws" {
  region = "eu-north-1"
}


# Узнаём, какие есть Дата центры в выбранном регионе
data "aws_availability_zones" "available" {}

# Ищем образ с последней версией Ubuntu
data "aws_ami" "ubuntu" {
  owners      = ["099720109477"]
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
}

# Созаём правило, которое будет разрешать трафик к нашим серверам
resource "aws_security_group" "web" {
  name = "Dynamic Security Group2"

  dynamic "ingress" {
    # Зададим правило, по каким портам можно обращаться к нашим серверам
    for_each = ["22", "80", "8080", "9100", "9080"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Web access for Application"
  }
}

resource "aws_launch_configuration" "web" {
  name_prefix = "dev-"
  image_id = data.aws_ami.ubuntu.id
  instance_type = "t3.micro"
  security_groups = [aws_security_group.web.id]
  user_data = file("user_data.sh")
  key_name = "id_rsa"
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "web" {
  name                 = "ASG-${aws_launch_configuration.web.name}"
  launch_configuration = aws_launch_configuration.web.name
  min_size             = 1
  max_size             = 1
  min_elb_capacity     = 1
  health_check_type    = "ELB"
  vpc_zone_identifier = [aws_default_subnet.availability_zone_1.id, aws_default_subnet.availability_zone_2.id]
  load_balancers = [aws_elb.web.name]

  dynamic "tag" {
    for_each = {
      Name = "Development Server in ASG"
    }
    content {
      key                 = tag.key
      value               = tag.value
      propagate_at_launch = true
    }
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_elb" "web" {
  name = "ELB"
  availability_zones = [data.aws_availability_zones.available.names[0], data.aws_availability_zones.available.names[1]]
  security_groups    = [aws_security_group.web.id]
  listener {
    lb_port           = 80
    lb_protocol       = "http"
    instance_port     = 8080
    instance_protocol = "http"
  }
  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 10
    timeout             = 60
    target              = "HTTP:80/"
    interval            = 70
  }
  tags = {
    Name = "Nginx-Highly-Available-ELB"
  }
}

resource "aws_default_subnet" "availability_zone_1" {
  availability_zone = data.aws_availability_zones.available.names[0]
}

resource "aws_default_subnet" "availability_zone_2" {
  availability_zone = data.aws_availability_zones.available.names[1]
}

#resource "aws_eip" "my_static_ip" {
#  instance = aws_instance.my_webserver.id
#  tags = {
#    Name = "Web server"
#  }
#}


# Запускаем инстанс
#resource "aws_instance" "my_webserver" {
#  # с выбранным образом 
#  ami = data.aws_ami.ubuntu.id
#  # и размером (количество ЦПУ и памяти зависит от этой директивы) 
#  instance_type          = "t3.micro"
#  vpc_security_group_ids = [aws_security_group.web.id]
#  key_name               = "id_rsa"
#  tags = {
#    Name = "Webserver"
#    Env  = "Production"
#    Tier = "Backend"
#    CM   = "Ansible"
#  }
#
#  lifecycle {
#    create_before_destroy = true
#  }
#
#}

# Выведем в консоль DNS имя нашего сервера 
output "web_loadbalancer_url" {
  value = aws_elb.web.dns_name
}

# Выведем IP адрес сервера
#output "my_web_site_ip" {
#  description = "Elatic IP address assigned to our balancer"
#  value       = aws_eip.my_static_ip.public_ip
#}
