data "digitalocean_domain" "default" {
  name       = var.domain
}

resource "digitalocean_record" "dev" {
  domain = data.digitalocean_domain.default.name
  type   = "CNAME"
  name   = "dev"
  ttl	 = "900"
  value  = "${aws_elb.web.dns_name}."
}
