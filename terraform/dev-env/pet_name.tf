resource "random_pet" "pet_name" {
  length    = 2
  separator = "-"
  keepers = {
    dns_name = "${aws_elb.web.dns_name}"
  }
}

