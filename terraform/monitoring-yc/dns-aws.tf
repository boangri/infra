data "aws_route53_zone" "domain" {
  name         = "${var.domain}."
  private_zone = false
}

resource "aws_route53_record" "monitoring" {
  zone_id = data.aws_route53_zone.domain.zone_id 
  name    = "monitoring"
  type    = "A"
  ttl     = "1800"
  records = [yandex_compute_instance.monitoring.network_interface.0.nat_ip_address]
}

resource "aws_route53_record" "grafana" {
  zone_id = data.aws_route53_zone.domain.zone_id
  name    = "grafana"
  type    = "A"
  ttl     = "1800"
  records = [yandex_compute_instance.monitoring.network_interface.0.nat_ip_address]
}
