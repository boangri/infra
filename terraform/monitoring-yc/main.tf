resource "yandex_compute_instance" "monitoring" {
  name        = "monitoring"
  hostname        = "monitoring"
  platform_id = "standard-v1"
  zone        = "ru-central1-a"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id =  "fd8eq6slbg844rhdtn0k"
    }
  }

  network_interface {
    subnet_id = "${yandex_vpc_subnet.subnet-1.id}"
      nat                = true
      nat_ip_address     = "62.84.112.209"
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }
}

resource "yandex_vpc_network" "monitoring" {
  name = "monitoring-network"
}

resource "yandex_vpc_subnet" "subnet-1" {
  name 	     = "monitoring-subnet"
  zone       = "ru-central1-a"
  network_id = "${yandex_vpc_network.monitoring.id}"
  v4_cidr_blocks = ["10.2.0.0/16"]
}

output "internal_ip_address_vm_1" {
  value = yandex_compute_instance.monitoring.network_interface.0.ip_address
}

output "external_ip_address_vm_1" {
  value = yandex_compute_instance.monitoring.network_interface.0.nat_ip_address
}

