data "aws_route53_zone" "domain" {
  name         = "${var.domain}."
  private_zone = false
}

resource "aws_route53_record" "gitlab_runner" {
  zone_id = data.aws_route53_zone.domain.zone_id  #var.zone_id
  name    = "gitlab-runner"
  type    = "A"
  ttl     = "1800"
  records = [aws_eip.runner_ip.public_ip]
}

