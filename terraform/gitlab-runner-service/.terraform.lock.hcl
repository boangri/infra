# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/aws" {
  version = "3.57.0"
  hashes = [
    "h1:Ki9PSbXeuSRdjijH8TYqitfPZax6Xk1eaFst2LslnHI=",
    "zh:241a4203078ea35f63202b613f0e4b428a842734ded62d9f487cdf7c2a66d639",
    "zh:2c1cbf3cd03a2a7ff267be09cedf1698738c372b1411ca74cfcb3bf4b0846f27",
    "zh:318ad2331f60e03d284f90f728486b9df7ac9570af641c43b56216357e624b52",
    "zh:43ff96b34b4829a34693281492786b9ca6dd06870dd45b0ae82ea352c33353d7",
    "zh:6c36b874622603793fc637272742d84ecbf68dfe4c8d8148bb6e9b733cd0e216",
    "zh:7a1aaac01c82d06f9ebc997ae2094a7d96e7a467aaaeaa1cda64ee952f3144d8",
    "zh:9b917b03b8771f87a021fe7aa9fd00ae06cc455a1eaa1fb748930182617b2772",
    "zh:bd90550e6d9311092170f4935e42e91e6d8bed5241e41eca39fa4aeca28d9c6f",
    "zh:be5076ea705c174581fd616b118e0c17d15bd8ab0da1b3eee4f3fb6b11e78f2c",
    "zh:f4f0d13414c932ecf65ba92daab6e755c244dcb77b4be59a3ac18ba2f56cdc00",
    "zh:fa3575a23fd20ce00658977374491022c4c0c36a00260ebeebb0c3f3af4824aa",
  ]
}

provider "registry.terraform.io/yandex-cloud/yandex" {
  version = "0.63.1"
  hashes = [
    "h1:3l8/M8siCDomp0qs0yF6g6ALpBpPUhMMEayuqDMJKuQ=",
    "zh:27a4c0ec95ccc0e58d01fd358bc280dfb6e6711393a4141aa4501578e0edead7",
    "zh:3adca68a987c67b349dc08e45c509f3459be861bb0c10059d0b622efdc372dd9",
    "zh:72fb616affb4392efc58135743f3b388b2fd13336b58f8a462e33b93ed445d05",
    "zh:76bbd4bbcf42bdb767dd5335a10d98ec6d9c0ccb2219f8e93cae0ebee9fdecf9",
    "zh:773a476b720206cef13d0ae369050c136c3e4ab2c33a6c6cef1775b3d0281ea5",
    "zh:817699dd287e7b1ef65f483f96e30f0623c89552c84f0369c3edaaacc893c8b0",
    "zh:849fcf026399f5a8a99a1556680421cd59dd1edb8872c2c3eaae83e1ec25dac0",
    "zh:878d6954b6e73669a5b5542254d53d7ac7fd81d1cd1a3fe1014a8d90b4c521a4",
    "zh:a347f58f27b40f730d78f326f3ff3c8a4c8dc977612577f079eddcffb2f9dbad",
    "zh:c0b9ad8cdc153a7a666164a203b5d53caa2f7e5bc0b19afa4680dbc09bd629e7",
    "zh:d4bdd03bfbc4daa19525a61d0eaffb3b5a2b1a1993611fbde57d796ecbdde856",
    "zh:e16e20c07ae89bea435427510ab4047b78da7b5f1762e42b2192a2cb61c0e6d8",
    "zh:f71bb64b87c260044e6de0e0fee606be8e356d859498fbbbe5457d217b65ec09",
  ]
}
