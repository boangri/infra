provider "aws" {
  region = "eu-north-1"
}

variable "domain" {
  type = string
}

resource "aws_route53_zone" "domain" {
  name = var.domain
  lifecycle {
    prevent_destroy = true
  }
}
