terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

variable "domain" {
  type = string
}

variable "do_token" {
  type = string
}

provider "digitalocean" {
  token = var.do_token
}

# Create a new domain
resource "digitalocean_domain" "default" {
  name       = var.domain
}
