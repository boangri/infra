data "digitalocean_domain" "default" {
  name       = var.domain
}

resource "digitalocean_record" "prod" {
  domain = data.digitalocean_domain.default.name
  type   = "CNAME"
  name   = "prod"
  ttl	 = "900"
  value  = "${aws_elb.elb.dns_name}."
}
