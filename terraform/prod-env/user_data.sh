#!/bin/bash -xe
sudo apt update -y
sudo apt install nginx -y
sudo apt install net-tools -y
sudo apt install docker.io -y
sudo apt install docker-compose -y
systemctl start nginx
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDKAQlq6GMQAwLc0r1D8N1fzCOAEGLyMju13RzK7JFzJNmfmYWtkVk5sPooiAYSVEwPHRXc1PGBnTAXS1Ypgi2jMjF1cCHehpXywJlqJMZ3sb9Ii/tSWvRwzPpsIP04rHkGzKs/VmuEdo82kOF9zqUTJfo15sCcvjA9FJZVdT0uXOpNbJV6XvAxZ6/yJBM7jphfvEH4174ojoVO3FfapJzqeIqkJH9eebweSpvWKuAWFudumdA+2GM62q0O/6IpfXD8WSpg6Ucsm8IZWaEWQc8rbluHPOcZ3zSrn5l/ZHL3RXLeoLySDjXsb+tVrUKnEh5Mb/SzZKD9tB63gMksy3yB gitlab-runner@..." >> /home/ubuntu/.ssh/authorized_keys
