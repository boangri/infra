provider "aws" {
  region = "eu-north-1"
}


data "aws_availability_zones" "available" {}

data "aws_ami" "ubuntu" {
  owners      = ["099720109477"]
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
}

resource "aws_security_group" "sec" {
  name = "Dynamic Security Group"

  dynamic "ingress" {
    for_each = ["22", "80", "8080", "9100", "9080"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Web access for Application"
  }
}

resource "aws_launch_configuration" "alc" {
  name_prefix = "prod-"
  image_id = data.aws_ami.ubuntu.id
  instance_type = "t3.micro"
  security_groups = [aws_security_group.sec.id]
  user_data = file("user_data.sh")
  key_name = "id_rsa"
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "asg" {
  name                 = "ASG-${aws_launch_configuration.alc.name}"
  launch_configuration = aws_launch_configuration.alc.name
  min_size             = 2
  max_size             = 2
  min_elb_capacity     = 1
  health_check_type    = "ELB"
  vpc_zone_identifier = [aws_default_subnet.availability_zone_1.id, aws_default_subnet.availability_zone_2.id]
  load_balancers = [aws_elb.elb.name]

  dynamic "tag" {
    for_each = {
      Name = "Production Server in ASG"
    }
    content {
      key                 = tag.key
      value               = tag.value
      propagate_at_launch = true
    }
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_elb" "elb" {
  name = "Prod-ELB"
  availability_zones = [data.aws_availability_zones.available.names[0], data.aws_availability_zones.available.names[1]]
  security_groups    = [aws_security_group.sec.id]
  listener {
    lb_port           = 80
    lb_protocol       = "http"
    instance_port     = 8080
    instance_protocol = "http"
  }
  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 10
    timeout             = 60
    target              = "HTTP:80/"
    interval            = 120
  }
  tags = {
    Name = "Production-ELB"
  }
}

resource "aws_default_subnet" "availability_zone_1" {
  availability_zone = data.aws_availability_zones.available.names[0]
}

resource "aws_default_subnet" "availability_zone_2" {
  availability_zone = data.aws_availability_zones.available.names[1]
}

output "web_loadbalancer_url" {
  value = aws_elb.elb.dns_name
}

# Выведем IP адрес сервера
#output "my_web_site_ip" {
#  description = "Elatic IP address assigned to our balancer"
#  value       = aws_eip.my_static_ip.public_ip
#}
