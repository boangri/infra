data "aws_route53_zone" "domain" {
  name = "${var.domain}."
  private_zone = false
}

resource "aws_route53_record" "elb" {
  zone_id = data.aws_route53_zone.domain.zone_id
  name    = "prod"
  type    = "CNAME"
  ttl     = "300"
  records = [aws_elb.elb.dns_name]
}
