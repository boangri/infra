provider "aws" {
  region = "eu-north-1"
}


resource "aws_eip" "static_ip" {
  instance = aws_instance.monitoring.id
  tags = {
    Name  = "Monitoring IP"
  }
}

data "aws_ami" "ubuntu" {
  owners      = ["099720109477"]
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
}

resource "aws_instance" "monitoring" {
  # с выбранным образом 
  ami                    = data.aws_ami.ubuntu.id
  # и размером (количество ЦПУ и памяти зависит от этой директивы) 
  instance_type          = "t3.micro"
  vpc_security_group_ids = [aws_security_group.monitoring.id]
  user_data = file("user_data.sh")
  key_name = "id_rsa"
  tags = {
    AMI =  "${data.aws_ami.ubuntu.id}"
    Name  = "Monitoring Server"
    Env = "Production"
    Tier = "Frontend"
  }

  lifecycle {
    create_before_destroy = true
  }

}

resource "aws_security_group" "monitoring" {
  name        = "Monitoring Security Group"
  description = "Security group for accessing traffic to our Monitoring server"


  dynamic "ingress" {
    for_each = ["80", "22", "9090", "3000", "9093", "9094"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }


  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "Monitoring SecurityGroup"
  }
}

# Выведем IP адрес сервера
output "monitoring_ip" {
  description = "Elatic IP address assigned to our Monitoring:"
  value       = aws_eip.static_ip.public_ip
}

